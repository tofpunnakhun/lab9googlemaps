package th.ac.tu.siit.lab9googlemaps;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;

public class MainActivity extends Activity {
	
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		//map.setMyLocationEnabled(true);
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				currentLocation = l;
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
